package com.brevitaz.genaralquestions.ocjp;

public class TestStaticClass {
    static class A {
        void process() throws RuntimeException {
            throw new RuntimeException();
        }
    }

    static class B extends A {
        void process() {
            System.out.println("B ");
        }
    }

    public static void main(String[] args){
        A a=new B();
        a.process();
    }
}
