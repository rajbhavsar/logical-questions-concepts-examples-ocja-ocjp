package com.brevitaz.genaralquestions.ocjp;

interface TestA {
    void toString1();
    String toString();
}

public class AnonymousClassExample {
    public static void main(String[] args) {
        System.out.println(new TestA() {
            public void toString1() {

            }
        });
        System.out.println(new TestA() {
            @Override
            public void toString1() {

            }
            public String toString() {
                return "test";
            }
        });
    }
}
