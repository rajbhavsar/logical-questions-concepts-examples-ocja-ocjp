package com.brevitaz.genaralquestions.ocjp;

public class Gen<T> {
    private T object;

    public Gen(T object){
        this.object=object;
    }

    public T getObject() {
        return object;
    }

    public static void main(String[] args) {
        Gen<String> gen= new Gen<String>("Test");
        Gen<Integer> integerGen=new Gen<>(10);
        System.out.println(gen.getClass().getName() +" "+integerGen.getClass().getName());
    }
}
