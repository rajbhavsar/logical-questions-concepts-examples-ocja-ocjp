package com.brevitaz.genaralquestions.ocja;

public class ArrayExample {
    public static void main(String[] args) {
        int[] x={1,2,3,4,5};
        int y[] =x;
        System.out.println(y[3]);
        System.out.println(y[10]);
        printSomething(2,3);
//        printSomeArray(2,3); //you can the method using this type of args you should pass the array object
    }
    public static void printSomething(int... x){
        System.out.println(x);
    }public static void printSomeArray(int[] x){
        System.out.println(x);
    }

}
