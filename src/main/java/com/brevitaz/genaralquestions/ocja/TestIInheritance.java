package com.brevitaz.genaralquestions.ocja;

/**
 * Basic example of inheritance with polymorphism with respect to parent reference and child reference.
 */
class A{
    String name="A";
    String getName(){
        return name;
    }
    String greetings(){
        return "Class A";
    }
}
class B extends A{
    String name="B";
    String greetings(){
        return "Class B";
    }
   /* String getName(){
        return name;
    }*/
}
public class TestIInheritance {
    public static void main(String[] args) {
        A a = new A();
        A b= new B();
        System.out.println(a.greetings()+" has name "+a.getName());
        System.out.println(b.greetings()+" has name "+b.getName());
    }
}
