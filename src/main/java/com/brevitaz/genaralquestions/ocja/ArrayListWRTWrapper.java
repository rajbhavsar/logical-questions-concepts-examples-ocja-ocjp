package com.brevitaz.genaralquestions.ocja;

import java.util.ArrayList;
import java.util.List;

public class ArrayListWRTWrapper {
    public static void main(String[] args) {
        ArrayList<Integer> listIn = new ArrayList<>();
        listIn.add(2);
        listIn.add(1);
        listIn.add(0);
        System.out.println(listIn);
        listIn.remove(listIn.indexOf(0));
        System.out.println(listIn);

        List<Character> list = new ArrayList<>();
        list.add(0, 'V');
        list.add('T');
        list.add(1, 'E');
        list.add(3, 'O');

        if (list.contains('O')) {
            list.remove('O');
        }

        for (char ch : list) {
            System.out.print(ch);
        }
    }

    int i = '5';
    Character x = 1;
    char y=2;

}
