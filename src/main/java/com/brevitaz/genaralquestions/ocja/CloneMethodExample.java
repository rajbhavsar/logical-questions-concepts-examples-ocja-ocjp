package com.brevitaz.genaralquestions.ocja;

import java.util.ArrayList;

class Counter {
    int count;
    Counter(int count) {
        this.count = count;
    }

    public String toString() {
        return "Counter-" + count;
    }
}

public class CloneMethodExample {
    public static void main(String[] args) {
        ArrayList<Counter> original = new ArrayList<>();
        original.add(new Counter(10));

        ArrayList<Counter> cloned = (ArrayList<Counter>) original.clone();
        cloned.get(0).count = 5;

        System.out.println(original);


        ArrayList<Integer> original1= new ArrayList<>();
        original1.add(10);
        System.out.println(original1);
        ArrayList<Integer> cloned1= (ArrayList<Integer>) original1.clone();
        System.out.println(cloned1);
        cloned1.set(0,5);
        System.out.println(cloned1.get(0) != null);
        System.out.println(cloned1);
        System.out.println(original1);
        System.out.println( original1 == cloned1);


        int a = 100;
        System.out.println(-a++);
        System.out.println(a);

    }
}
