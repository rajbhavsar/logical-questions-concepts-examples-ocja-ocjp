package com.brevitaz.genaralquestions.ocja;

public class TestStaticMethodVars {
    private int counterX=0;
    private static int counter=0;
    public static int getInstanceCount(){
        // return counterX;
        //Non-static field 'counterX' can not be referenced from static context
        return counter;
    }
    public TestStaticMethodVars(){
        counter++;
    }
}
