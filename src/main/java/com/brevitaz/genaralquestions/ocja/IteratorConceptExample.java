package com.brevitaz.genaralquestions.ocja;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class IteratorConceptExample {
    public static void main(String[] args) {

        //main(args);

        List<String> dryFruits = new ArrayList<>();
        dryFruits.add("Walnut");
        dryFruits.add("Apricot");
        dryFruits.add("Almond");
        dryFruits.add("Date");

        Iterator<String> iterator = dryFruits.iterator();
        while (iterator.hasNext()) {
            String dryFruit = iterator.next();
            if (dryFruit.startsWith("A")) {
                iterator.remove();
            }
        }

        System.out.println(dryFruits);
    }
}
