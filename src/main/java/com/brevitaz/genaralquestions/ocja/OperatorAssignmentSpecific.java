package com.brevitaz.genaralquestions.ocja;

interface DeclareStuff {
    public static final int EASY = 3;
    void doStuff(int t);
}

public class OperatorAssignmentSpecific implements DeclareStuff {
    public static void main(String[] args) {
        int x = 5;
        new OperatorAssignmentSpecific().doStuff(++x);
    }
    @Override
    public void doStuff(int s)/*Because in interface by default the method access modifier is public if we are not
    declaring, that doesn't mean that we can override that method by using default access modifier*/ {
        s += EASY + ++s;
        System.out.println("s " + s);
    }
}
