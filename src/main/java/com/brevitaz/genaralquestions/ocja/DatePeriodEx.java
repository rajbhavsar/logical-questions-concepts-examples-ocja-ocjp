package com.brevitaz.genaralquestions.ocja;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class DatePeriodEx {
    public static void main(String[] args) {
        Period period= Period.of(2,3,0);
        System.out.println(period);

        Period period1= Period.ofMonths(4);
        System.out.println(period1);

        Period period2= Period.ofWeeks(2);
        System.out.println(period2);

        Period period3= Period.ofMonths(3).ofWeeks(3).ofDays(2).ofYears(3);
        System.out.println(period3);


        List<Integer> list = new ArrayList<Integer>();

        list.add(27);
        list.add(27);

        list.add(new Integer(27));
        list.add(new Integer(27));

        System.out.println(list.get(0) == list.get(1));
        System.out.println(list.get(2) == list.get(3));

        float d=10;
        double f=d;

        String[] arr={"A","A","C"};
        System.out.println(arr[1]);
        //new String("TUE");
        String[] arr1= {new String("X"),new String("Y")};

        System.out.println("A---->"+ (new String("X") == new String("X")));
        System.out.println(arr[1]==arr[0]);


        List<String> stringList= new ArrayList<>(50);
        stringList.add(10,"Raj");


    }
}
