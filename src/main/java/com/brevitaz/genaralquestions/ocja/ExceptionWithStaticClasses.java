package com.brevitaz.genaralquestions.ocja;


public class ExceptionWithStaticClasses {
    static class A {
        void process() throws Exception {
            throw new Exception();
        }
    }

    static class B extends A {
        void process() {
            System.out.println("B ");
        }
    }

    public static void main(String[] args) throws Exception {
        A a = new B();
        a.process();
        Integer integer=10;
    }
}
