package com.brevitaz.genaralquestions.ocja;

class Plant{
    private String name;
    public Plant(){
        this("Fern");
    }
    public Plant(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
}
class TreeX extends Plant{
/*
    public TreeX(String name) {
        super(name);
    }
*/

    public void growFruits(){}
    public void dropLeaves(){}
}
public class ConstructorChainingExample {
    public static void main(String[] args) {
    }
}
