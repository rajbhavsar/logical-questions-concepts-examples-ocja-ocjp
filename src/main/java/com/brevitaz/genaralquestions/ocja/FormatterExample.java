package com.brevitaz.genaralquestions.ocja;

public class FormatterExample {
    public static void main(String[] args) {
        System.out.format("Pi is approximately %d.", Math.PI);
    }
}
