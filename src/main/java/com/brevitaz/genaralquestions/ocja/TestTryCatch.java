package com.brevitaz.genaralquestions.ocja;

public class TestTryCatch {
    public static void parse(String str) {
        float f=10;
        try {
//            float f = Float.parseFloat(str);
            f = Float.parseFloat(str);
            System.out.println("In Try: "+f);
        }
        catch (NumberFormatException nfe) {
             f = 0;
            System.out.println("In Catch: "+f);
             }
        finally {
             System.out.println("In Finally: "+f);
             }
         }
        public static void main(String[] args) {
            parse("invalid");
            parse(null);
        }
    }
