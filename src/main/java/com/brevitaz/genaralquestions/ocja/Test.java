package com.brevitaz.genaralquestions.ocja;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        Object[] objects={
                new Integer(5),
                new String("Raj"),
                new Boolean(true),
                new Integer(12)
        };
        Arrays.sort(objects);//Exception in thread "main" java.lang.ClassCastException: java.lang.Integer cannot be cast to java.lang.String
    }
}
