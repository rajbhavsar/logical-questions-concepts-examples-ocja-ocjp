package com.brevitaz.genaralquestions.ocja;

public class EnumExample {
    enum Elements {
        EARTH, WIND,
        FIRE {
            public String info() {
                return "Hot";
            }
        };

        public String info() {
            return "elements";
        }
    }

    public static void main(String[] args) {
        System.out.println(Elements.FIRE);
        System.out.println(Elements.FIRE.info());
        System.out.println(Elements.EARTH.info());
    }
}
